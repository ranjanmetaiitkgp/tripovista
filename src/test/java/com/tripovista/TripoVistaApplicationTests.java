package com.tripovista;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tripovista.domain.Address;
import com.tripovista.domain.CompanyDetails;
import com.tripovista.domain.CompanyGroup;
import com.tripovista.repository.AddressRepository;
import com.tripovista.repository.CompanyDetailsRepository;
import com.tripovista.repository.CompanyGroupRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TripoVistaApplication.class)
public class TripoVistaApplicationTests {
	

	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private CompanyGroupRepository companyGroupRepository;
	
	@Autowired
	private CompanyDetailsRepository companyDetailsRepository;
	
	@Test
	public void contextLoads() {
		
		CompanyGroup cg = new CompanyGroup();
		
		Set<Address> addressList1 = new HashSet<>();
		Address add = new Address();
		add.setAddressLine1("Line 1");
		add.setAddressLine2("Line 2");
		add.setCity("Bangalore");
		add.setState("KA");
		add.setPinCode(560078);
		
		
		Address add1 = new Address();
		add1.setAddressLine1("Noida Line 1");
		add1.setAddressLine2("Noida Line 2");
		add1.setCity("Noida");
		add1.setState("UP");
		add1.setPinCode(201301);
		
		addressRepository.save(add);
		addressRepository.save(add1);
		
		addressList1.add(add);
		addressList1.add(add1);
		
		cg.setGroupName("DulDul Group of Companies.");
		//cg.setAddress(addressList1);
		cg.setCreatedOn(new Date());
		
		companyGroupRepository.save(cg);
		
		CompanyDetails cd = new CompanyDetails();
		cd.setCompanyName("DulDul Enterprises");
		cd.setCompanyGroup(cg);
		//cd.setAddress(addressList1);
		cd.setStatus((short)1);
		cd.setCreatedOn(new Date());
		
		companyDetailsRepository.save(cd);
		
		CompanyDetails cd1 = new CompanyDetails();
		cd1.setCompanyName("DulDul Locomotives");
		cd1.setCompanyGroup(cg);
		//cd1.setAddress(addressList1);
		cd1.setStatus((short)1);
		cd1.setCreatedOn(new Date());
		
		companyDetailsRepository.save(cd1);
		
	}

}
