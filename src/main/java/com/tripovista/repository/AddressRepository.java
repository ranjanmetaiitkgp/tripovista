package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{

}
