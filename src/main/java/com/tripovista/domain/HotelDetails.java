package com.tripovista.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="hotel_details")
public class HotelDetails implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4546021636129656581L;
	
	private Long hotelId;
	private String hotelName;
	private Address address;
	private short status;
	private Date createdOn;
	private Date modifiedOn;
	private HotelGroup hotelGroup;
	
	public Long getHotelId() {
		return hotelId;
	}
	public void setHotelId(Long hotelId) {
		this.hotelId = hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	
	public short getStatus() {
		return status;
	}
	public void setStatus(short status) {
		this.status = status;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public HotelGroup getHotelGroup() {
		return hotelGroup;
	}
	public void setHotelGroup(HotelGroup hotelGroup) {
		this.hotelGroup = hotelGroup;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

}
