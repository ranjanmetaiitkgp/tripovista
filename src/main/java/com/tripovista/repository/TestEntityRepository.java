package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.TestEntity;

public interface TestEntityRepository extends CrudRepository<TestEntity, Long>{

}
