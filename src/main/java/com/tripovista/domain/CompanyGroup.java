package com.tripovista.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="company_group")
public class CompanyGroup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -971620117083353744L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="company_group_id")
	private Long companyGroupId;
	
	@Column(name="group_name")
	private String groupName;
	
	@OneToOne
	private Address address;
	
	@OneToMany(mappedBy="companyGroup")
	private Set<CompanyDetails> companyList;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="modified_on")
	private Date modifiedOn;

	public Long getCompanyGroupId() {
		return companyGroupId;
	}

	public void setCompanyGroupId(Long companyGroupId) {
		this.companyGroupId = companyGroupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Set<CompanyDetails> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(Set<CompanyDetails> companyList) {
		this.companyList = companyList;
	}

}
