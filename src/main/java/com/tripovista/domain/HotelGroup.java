package com.tripovista.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hotel_group")
public class HotelGroup implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4745863825821251305L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long hotelGroupId;
	
	@Column(name="hotel_name")
	private String hotelName;
	private Address address;
	private Date createdOn;
	private Date modifiedOn;
	private short status;
	private Set<HotelDetails> hotelList;
	
	public Long getHotelGroupId() {
		return hotelGroupId;
	}
	public void setHotelGroupId(Long hotelGroupId) {
		this.hotelGroupId = hotelGroupId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public short getStatus() {
		return status;
	}
	public void setStatus(short status) {
		this.status = status;
	}
	public Set<HotelDetails> getHotelList() {
		return hotelList;
	}
	public void setHotelList(Set<HotelDetails> hotelList) {
		this.hotelList = hotelList;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}

}
