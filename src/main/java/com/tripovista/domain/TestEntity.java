package com.tripovista.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="test_entity")
public class TestEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8354630342392210084L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="entity_id")
	private Long entityId;
	
	@Column(name="entity_name")
	private String entityName;
	
	@Column(name="entity_desc")
	private String entityDescription;
	
	public Long getEntityId() {
		return entityId;
	}
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}
	public String getEntityName() {
		return entityName;
	}
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	public String getEntityDescription() {
		return entityDescription;
	}
	public void setEntityDescription(String entityDescription) {
		this.entityDescription = entityDescription;
	}

}
