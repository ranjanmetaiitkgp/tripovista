package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.CompanyGroup;

public interface CompanyGroupRepository extends CrudRepository<CompanyGroup, Long>{

}
