package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.CompanyDetails;

public interface CompanyDetailsRepository extends CrudRepository<CompanyDetails, Long>{

}
