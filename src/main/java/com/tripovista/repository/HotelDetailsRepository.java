package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.HotelDetails;

public interface HotelDetailsRepository extends CrudRepository<HotelDetails, Long>{

}
