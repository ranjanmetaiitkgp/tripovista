package com.tripovista.repository;

import org.springframework.data.repository.CrudRepository;

import com.tripovista.domain.HotelGroup;

public interface HotelGroupRepository extends CrudRepository<HotelGroup, Long>{

}
